+++
title: March 2024 Gaming
desc: The games I played in March 2024
date: Sun, 31 Mar 2024 00:00:00 EDT
tags: gaming-list, gaming, 2024, review
+++

## Sonic Adventure 2 - (2002) - (3/5)
![](sab2.png)

I’m a veteran of Sonic Adventure 2. Played the game in 2002 and it got me hooked on Sonic the Hedgehog. Been chasing that high ever since as just about every other Sonic game I’ve played never hit the peaks that this game hit for me back then. But honestly, my dedication to this game has been wavering in recent years.

So the game is split into two different story arcs, you play as either the Heroes (Sonic, Tails, and Knuckles) or the Villains (Shadow, Eggman, and Rouge). There are three types of levels based on each character, Sonic/Shadow levels find you quickly moving through stages, doing loops, grinding rails, etc. which is close to the normal Sonic gameplay. Tails/Eggman levels are slower paced as you maneuver their mechs through levels blasting the shit out of everything that moves. Knuckles/Rogue levels involve searching for three objects hidden in the level, using vague hints and a hot/cold system to locate the items. Each level contains four extra missions in addition to the story mode that have different completion objectives.This game also features so many side-games/mini-games.

There’s cart racing, multiplayer battles, and the penultimate game mode that everyone remembers fondly: Chao Garden. The Chao Garden involves raising little creatures by giving them food, animals, and Chaos Drives to boost their stats. You then enter them into competitions like races and karate tournaments. This is the heart and soul of the game. Any frustrating moments you have from the Story/Stages culminates in spending time with your children and watching them grow.

Overall the game feels janky in today’s age. The camera actively fights against you, controls don’t feel too responsive, and the audio/video will never be synced correctly when playing on modern hardware (don’t even remember it being correct on Gamecube). That being said, my nostalgia for the title carries me to deal with the jank and enjoy the blast from the past. If you want a relic from the 2000’s, this game really encapsulates it between cool set pieces, punk rock, rap music, and the edginess many sought after.

## Nioh 2 - (2020) - (3/5)
![](nioh2.png)

Ye Olden Nippon Dark Souls. In Nioh 2 you take the role of a “shiftling” named Hide, a half human/Yokai hybrid who is able to utilize the cores/souls of Yokai to enhance their abilities. This is a custom character you create with a crazy amount of depth for the character creator. And if character creating isn’t your thing, there’s a large amount of presets to pick from so you don’t waste a ton of time. You join forces with a funny monkey man and his funny monkey as you get involved in the warfare of shoguns and the like, all while dispatching Yokai between.

Like the first one, this game is H U G E. Tons of missions, tons of skills, tons of gear with set bonuses, etc. This latest entry features multiple types of Devil Trigger (DT), and enemy moves you can add to your arsenal. This game also features 11 melee weapons and 3 ranged weapons to utilize all with their own move sets, and scaling. You’ll also have access to Ninjutsu weapons (shuriken, kunai, bombs, etc.), as well as Omnyo magics (Talisman to imbue your weapon with elements, throwable projectiles, curative spells, debuffs for your enemies, etc.). Needless to say you have MANY ways you can customize your playstyle in this game.I myself opted to use the Switch Glaive weapon (It’s a weapon that folds from a quick sword, into a medium spear, into a heavy scythe) and cast Onmyo magics (mostly just the projectiles).

The gameplay loop goes as follows: Enter a mission, explore the level, kill some enemies, find checkpoints, fight a boss.During all of this you’ll likely get bopped and defeated a lot. Every encounter can be deadly as enemies do a lot of damage, and are unrelenting in their moves. The pace of this game is fast, way faster than my mind/body could keep up with initially as I fumbled controls and reactions. However as you progress and you figure out what moves you have in your arsenal, it becomes a very delicate dance of dipping in and out of danger and parrying big attacks to create openings to strikes. This is a lot more applicable for the Yokai enemies as the human enemies seem to be pretty much a joke in comparison and can be hit-stunned to death.

At the moment I am far from completing this game. So far I have been enjoying my time with the game. The game is fun to play and the dopamine hits I get from making progress offset the frustrations of dying to the same enemies over and over. I don’t know if I care for the story as I don’t really know exactly what’s happening but that’s fine. I am definitely overwhelmed by the amount of gear you find, and knowing what’s worth looking at and what’s worth ignoring. At the moment I’m just wearing whatever is lightweight and gives the most defense, and finding whichever Switch Glaive does the most damage.

Overall I would recommend this to fans of challenging games and Souls-like games in particular. If you come from playing the FromSoftware series, this will feel close enough to get your foot in the door but different enough to differentiate from what you’ve played prior. If you’re looking to get into Souls-like games, I find this one is harder to recommend because it feels a lot crazier than the other ones I’ve played (Demon/Dark Souls 1-3, Elden Ring, Bloodborne, The Surge, Stranger of Paradise).

## Heretic’s Fork - (2023) - (4/5)
![](heretics_fork.png)

Deck building tower defense game. The aesthetic of this game is top notch, between the desktop main menu, all the killer music in the game, and the crazy pixel sprites for enemies, towers, and HUD elements. There’s a healthy selection of different characters that each have different abilities, and alternate costumes that change their stats even further. The towers/cards you unlock allow for different play styles and ways to build your strategy. My favorite is using buildings that summon explosive units that suicide bomb the foes. Unlockable cards expand upon the builds you are given early on and lead to much more powerful combos. I don’t really have much else to say, or anything negative about this game other than maybe early on builds can feel kind of samey, but once you figure out the mechanics, unlock better cards, and get in the groove, it’s a blast.

I would recommend this game to tower defense fans, deck building/card game fans, and people who enjoy cheap indie games. If nothing else, look up the soundtrack of this game and start listening to it. All the music really rocks, even the DLC pack featuring Disturbed.

## Broken Sword 1 - Shadow of the Templars: Director’s Cut - (1996/2010) - (4/5)
![](broken_sword1.png)

A Point-and-Click Adventure classic from 1996. Where Sierra and LucasArts dominated the scene with the likes of King's Quest and Monkey Island, Broken Sword by Revolution aimed to make its mark with hand drawn graphics, and a complex storyline, while still including charming characters.

The game follows American tourist George Stobbart, as he witnesses a clown steal a briefcase and bomb a cafe he's sitting outside of. From here, George meets photo-journalist Nico Collard who has been investigating a "Costume Killer" and believes the clown is related. This kick starts their adventure as they uncover ancient mysteries and conspiracies.

Most of the gameplay involves finding items, talking to characters, and figuring out how to mix them together to advance the plot. I will preface that these kind of games are really not my forte as I usually find a lot of the problem solving to be unintuitive or beyond my thinking. I would spend a lot of time trying to use every item with everything I could interact with. It would have been nice if your inventory would empty of items you no longer needed to help eliminate that problem solving. The nice thing about the Director's Cut version I played was the hint system in the menu. It would start by giving riddles or small hints to flat out telling you the solution (which I would definitely need at times).

While the gameplay isn't exactly my thing, I didn't find myself frustrated with getting lost because I really enjoyed the story and characters. I would find myself wondering where the plot was going next as slowly piece by piece things started to come together. I would also look forward to see what new interactions George would have as he would maneuver his way through the different scenarios ranging from minute to over the top. The things he gets away with is always really fun/funny to watch unfold. The director's cut also includes some additional scenarios where you play as Nico to give her some backstory. Largely it feels disconnected but it broke up the story a bit and didn't overstay its welcome.

Overall, I do think this game being on the older end may turn off modern gamers from wanting to play it themselves. But even with all its shortcomings, I did enjoy my time with this game a lot. It's a genre I haven't really explored (outside of Putt-Putt and other Humongous Entertainment children's point-and-click games), and it was a nice change of pace from my usual gaming. I was able to get all five Broken Sword games on sale for about $5 and look forward to continuing this series and see the good and bad that comes with a game series from the 90's transitioning to accommodate gaming standards throughout the yea If you enjoy story based games or retro PC games then I think Broken Sword is definitely worth a look at.

## SaGa Frontier Remastered - (1997/2021) - (x/5)
![](saga_frontier_remastered.png)

Crazy ass RPG. I hope to come back and figure out what the hell has been happening with this one.

## The Incredible Adventures of Van Helsing: Final Cut - (2015) - (3/5)
![](van_helsing.png)

I first played this Diablo-like action RPG back in 2016. In my time with it I remember it being fine, but not feeling like anything too special. Now I’m playing co-op with a friend 8 years later and this game is much more enjoyable.

The Incredible Adventures of Van Helsing: Final Cut is a combination of a trilogy of games. You play as monster hunter Van Helsing as you explore Borgovia with your ghost companion Lady Katarina, fighting creatures of all sorts and helping folks. There are 6 playable classes with a large selection of skills to use on each. In addition, each skill features three “Rage” abilities that can be used to augment the move. Your companion Lady Katarina can be built in a number of ways to compliment your build or enhance your own build. It’s definitely funny in co-op as there is one Katarina for each player.

The first time I played as a gunslinger and remember just shooting my basic attack from a million miles away, dispatching everything with ease. Now I’m playing as the protector and jumping into the fray with sword and shield, deflecting attacks while my partner in crime is a constructor, summoning a bunch of robot automatons who blast the foes I keep at bay. It’s a fun dynamic, I enjoy my boring build and he feels like he’s having enough fun for the two of us.

My biggest gripe with this game is how slow it is. You move really slow, and areas feel pretty big. And because of this we try not to stray too far from the main path as exploring leads to a lot of dead air on the return trip. I wish maybe the areas were more linear or looped around better so you’re not left with lulls in the action should you want to follow up on a side quest.

All in all, this game is very much like most other games in its genre. I don’t think it will do anything to WOW you where you’ll find yourself saying it’s your favorite, but I do think the setting, and funny dialogues make it an enjoyable playthrough. Also much better with a friend to survive the boring parts. Would recommend those interested in the genre and haven’t played it before, but I wouldn’t recommend it to ARPG fanatics.I think it’s a little too basic to satisfy the itch of veterans of say Path of Exile.
