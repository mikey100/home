+++
title: Castlevania: Portrait of Ruin
desc: Two Screens, Two Protagonists
date: Fri, 6 Sep 2024 19:33:00 EDT
thumbnail: portrait_of_ruin.png
thumbnail_desc: Castlevania: Portrait of Ruin
tags: gaming, 2024, review
+++

The second Castelvania title on the Nintendo DS. Out of the three DS games, Portrait of Ruin was always the one I had the most interest in. We have not one but two new characters to the series, you get to play as both of them and freely switch between them, and one of the characters isn't a traditional Castlevania combatant. So now that the [Castlevania: Dominus Collection](https://store.steampowered.com/app/2369900/Castlevania_Dominus_Collection/) is out and I have a way to play Portrait of Ruin on my Steam Deck, was it worth the wait?

# Basics:

It's another modern Castlevania entry! You explore a labryinth like castle, acquiring upgrades that help you reach new parts of the castle all while dispatching foes for XP points, finding weapons and gear to boost your power.

# Plot:

Portrait of Ruin starts out with our two protagonists: Jonathan Morris and Charlotte Aulin heading towards the revived Dracula's Castle in an attempt to defeat the Dark Lord before he fully resurrects and brings about the world's ruin. Jonathan is the son of previous Castlevania protagonist John Morris from Castlevania: Bloodlines. Charlotte is a young sorceress descended from Belnades clan of magicians and witches. Jonathan is in possession of the legendary Vampire Killer whip but unfortunately doesn't know how to use it to its full potential.

They meet up with a member of the church, Vincent, who is tasked with assisting in the destruction of the Castle. And shortly after entering the Castle, Jonathan and Charlotte meet a ghost by the name of Wind who provides the pair his knowledge and skills. Wind reveals that the castle is technically Dracula's, but the current master isn't Dracula but a vampire painter named Brauner who infuses magic into paintings to increase his power. Upon coming across the first Portrait, Charlotte recognizes how the magic of these paintings works. She explains that they can enter the paintings, destroy the evil lurking within them and it should weaken the Lord of the Castle. Thus starts their main objective of weakening Brauner to defeat him and stop the resurrection of Dracula.

The story is the same we see a lot of these games. I don't think Castlevania is exactly known a series known for having a phenomenal story, and this one is no different. The tried and true motivation of preventing Dracula's is enough for me. The characters of Jonathan and Charlotte are younger and part of their development is growing up, whether that's Jonathan's attempt to move past the anger he harbors against his father for not teaching him how to use the Vampire Killer, or Charlotte proving herself as a powerful magic user. I think a lot of people can relate to the teenage angst of Jonathan or Charlotte's goal of being treated like and adult.

![Also Jonathan uses the phrase: "No problemo" a lot. I loved reading it every time he said it!](no_problemo.jpg)

# Gameplay Overview

In this entry you play as two characters. While both play very similar, they do have differences. Jonathan plays closer to previous protagonists like Alucard and Soma Cruz where he has access to different weapons, but has skills that work like the subweapons of the Belmont characters. Charlotte only has one type of weapon, but learns multiple spells which can be charged for greater effects. In addition the two have different stat growths and have access to different gear that tends to further those stat differences. Learning these new skills/spells is similar to Aria/Dawn of Sorrow as defeating enemies can reward players with them, and a number of Charlotte's spells feel like the moves you'd learn from the Aria/Dawn of Sorrow Souls. Also returning are Weapon Crushes in the form of Dual Crushes in which Jonathan and Charlotte team up to use a spell similar to previous Weapon Crushes such as Hydro Storm or Thousand Edge. Instead of destroying a sub weapon though, it just requires a lot of mana to cast.

![Our heroes assaulting an Axe Armor in his own home!](gameplay.jpg)

While you can play as and freely switch between either character, this game also allows you to have the non player controlled character to be out as an active NPC that follows the player and assists them in combat. This mechanic is also used to solve a handful of puzzles as you're able to command them to stand on switches, push heavy objects together, or use them as an extra jump by footstooling off them. Unfortunately there are very few times I think the puzzles that require both characters feels distinct but the one or two notable times it does I think it's really cool. Otherwise it's doing what previous characters can do alone but with extra steps.

In Portait of Ruin you explore Dracula's Castle as well as a variety of different worlds inside paintings that you find along the way. I find the biggest thing that thing that sticks out the most when playing a Castlevania game is how enjoyable it is to traverse and backtrack through the maze like Castle. For the most part, I think this Castle is pretty inoffensive overall. This can be in part due to the Portraits being separate maps/locations so you get bite sized sections that feel completely separate from the main castle. That being said, I hate the circus level. Love the Egypt level though! Thankfully the main castle isn't overly large and has a ton of teleporter rooms throughout so backtracking to different Portraits, or taking a pit stop at merchant or a save room is not a hassle and you can easily get back where you left off.

![Woah! There's a whole world in here!](entering_portrait.jpg)


# The Verdict:

Portrait of Ruin is another solid entry in the modern Castlevania entries. Having the two playable characters doesn't add a ton of difference or variety in the gameplay, but it has enough meaningful moments. Having the two characters with their own moves allows you to kit them for different scenarios such as equipping Jonathan with weapons/skills that do a certain type of damage while Charlotte can cover a different weakness with her spells. Or you can double down on a single strategy using the ability to call in your partner to boost your active character.

If you like platformer games, action RPGs, or want to delve into a piece of a longstanding series history, I can see Portrait of Ruin being a great first time entry. You get a lot of the series basics in this that will make other entries seem familiar, but the dual character setup makes Portrait of Ruin stand out as its own game instead of a better/lesser version of other Castlevania entries.

