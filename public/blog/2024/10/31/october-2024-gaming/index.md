+++
title: October 2024 Gaming
desc: The games I played in October 2024
date: Thu, 31 Oct 2024 00:00:00 EDT
tags: gaming-list, gaming, 2024, review
+++

## Planet of the Eyes - (2015) - (3/5)
![](planet_of_the_eyes.png)

You play as a little robot that crash lands on a mysterious alien planet. The game is very simple and is reminiscent of LIMBO. You move mostly to the right to progress as you solve some minor puzzles like pushing blocks to reach ledges, holding down buttons to fill a room with water, and faking out enemies to get around them.

There's a story told through audio logs you find, but I wasn't really paying attention to them when I played. This game is super short, can be beat in about an hour. If you can find this game for really cheap, I'd say sure check it out. But otherwise there really isn't much to this game. 

## Hexologic - (2018) - (4/5)
![](hexologic.png)

Hexologic is a Sudoku based puzzle game. You fill hexagons with numbers 1, 2, or 3 to try to make the line of hexagons count to the number at the end of it. Later on this gets more tricky as you have more conditions to meet, hexagons that add up into other hexagons, etc. 

There's about 100 levels with two difficulties for each level, and a few randomized levels that are replayable. I was able to 100% the game in about 7 hours. I had a lot of fun playing this while listening to an audiobook, so I would definitely recommend this. Because of it's shorter playtime and less involvement, I don't know if I would recommend buying it full price, but even then at it's $5 MSRP, you can't really go wrong.

## Metaphor: ReFantazio - (2024) - (3.5/5)
![](metaphor_refantazio.png)

Another ATLUS JRPG from the studio that makes Persona. Metaphor: ReFantazio sees you take on the role of a boy trying to rid his friend and Prince of the nation of a deadly curse that will end his life. You'll engage in turn based combat similar to other ATLUS games, explore dungeons filled with deadly monsters and riches, form bonds with people to enhance your strength a la Persona, and immerse yourself in a fantasy world with problems not too dissimilar to our own.

I'm still playing through this game and will have a full review when I finish it. But at the moment, I am enjoying Metaphor and would recommend it to people who enjoy this type of game or maybe want to get into this type of game. But for the hardcore JRPG fans, for better or worse Metaphor is more of the same.


## CLICKOLDING - (2024) - (2/5)
![](clickolding.png)

CLICKOLDING is a short form game about a man who is going to to pay you $14,000 to watch you click a four-digit clicker 10,000 times causing it to reset. The man gives you instructions on how to click like faster/slower but will also stop you from clicking if you're not standing in the right spot or looking at the right part of the room. I can't say I really got anything out of this game though. The game is silly by nature because yes all you do is click for the most part. I kind of went in expecting to see something more and that's on me. I wish there was a bit more dialogue to keep you engaged because clicking 10,000 times takes a while, and maybe would have made up for the unfulfilling game. The best I got is a meta narrative about streamer culture where people pay money to streamers to entertain them, whether that's playing video games, pretending to be AI, XXX adult activites, and what not. Boiled down to the basics, those activities are no different than clicking a clicker in front of a stranger willing to pay you money to do so. But that could also just be me reaching for something not there as I try to justify my short time with CLICKOLDING.

## Mouthwashing - (2024) - (4/5)
![](mouthwashing.png)

After a crash that causes a space freighter ship delivering mouth wash to be stranded, the game follows the crew as they deal with the fallout of the crash as well as flashbacks to the events leading up to it. Mouthwashing is mostly a narrative game where you mostly follow the co-pilot Jimmy's perspective. In each chapter of the game you are usually given a task like open a locked door, find an object, give the injured Captain Curly his medicine, and so on. As you go through these tasks you interact with the other survivors of the crew to get items or information. All the while, you get to understand more about the crew, their thoughts on the situations that come up, as well as their feelings about life in general.

To go into more detail of the story would kind of spoil it but there's still plenty to talk about. As far as gameplay mechanics go, you explore the ship in a first person, solve some puzzles like combining items, or entering in codes. There's a code scanner in the form of a blacklight flashlight that when shined on certain parts shows a code. This is meant to keep things out of reach of the crew as only the Captain has a code scanner. The visuals of this game is fantastic to say the least. I'm a big sucker for PS1 graphics games, and the amount of indie games going this route has been much appreciated. Something about the detailed but low fidelity textures on these low polygon count models really appeals to me, and is a great use for these kinds of surreal horror games. They also have some in game TV's with some cartoons, and footage that were very fun to watch. I'm also a fan of the soundtrack now after playing through the game. Some songs are defintiely not for causal listening as it's spooky noises, but songs like Wake Rock and Dragon Breath Theme are great!

One criticism I had is there's a few sections of the game where you're required to do actual gameplay that isn't walking around and talking to people. In the sections, you can "lose" in which you're quickly reset to a checkpoint. These sections are given little instruction and it's not abundently clear what you're supposed to be doing or where you're supposed to be going. So while playing with my group, we found these more frustrating than scary or engaging. These sections thankfully aren't too long but it would have been nice if they were fleshed out a bit more.

All in all, I think Mouthwashing is a great horror experience. Early on it explores some themes that have been resonating a lot with my peers and I this year like the dread comes from feeling like you could be doing better at a different job, not feeling skilled enough to make it anywhere else, or working a job you hate but is stable enough to keep you there while you sort of your personal life (It also explores a lot of other themes that weren't so relateable but make for a great horror narrative). We clocked in about 2.5 hours to complete the main game, there's plenty of achievements we missed that will give me some more Mouthwashing time, and I probably will be thinking about this game for the next month or so. I would recommend this to anyone looking for a horror game, people who enjoy narrative games, and indie gamers looking for a new experience.
