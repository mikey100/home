+++
title: Mikey's Reading List
desc: A list of all the books I've read with scores.
date: Wed, 16 Oct 2024 13:12:21 EDT
+++

In 2018 I found myself wanting to start reading for fun. If it wasn't obvious, [I play a ton of videogames!!!](/gaming-list/2024-08-28-gaming-list) So I started reading on and off until recently where I'm now an avid reader. I'm a big fan of fantasy novels (much to my dismay at times), but also enjoy reading thriller and horror novels as well.

Below is the list of all the books I've read organized first by the author's last name, then by year it was published. I will also try to keep books in a single series grouped together.

## The List

Alice Feeney
 - Rock Paper Scissors

Lucy Foley
 - The Guest List

Colleen Hoover
 - Verity

Lisa Jewell
 - Then She Was Gone

Marcus Kliewer
 - We Used to Live Here

Jay Kristoff
 - Empire of the Vampire

Shari Lapena
 - The Couple Next Door

Cixin Liu
 - The Three-Body Problem

Claudia Lux
 - Sign Here

Sarah J. Mass
 - Throne of Glass
    - The Assassin’s Blade
    - Throne of Glass
    - Crown of Midnight
    - Heir of FIre
    - Queen of Shadows
    - Empire of Storms
    - Tower of Dawn
    - Kingdom of Ash
 - A Court of Thorns and Roses
    - A Court of Thorns and Roses
    - A Court of Mist and Fury
    - A Court of Wings and Ruin

Freida McFadden
 - Do Not Disturb
 - Ward D

Tamsyn Muir
 - Gideon the Ninth

Terry Pratchett
 - Color of Magic

Taylor Jenkins Reid
 - Daisy Jones & the Six

Riley Sager
 - Lock Every Door

Nicola Sanders
 - All The Lies
 - Don't Let Her Stay

Brandon Sanderson
 - Mistborn
    - The Final Empire
    - The Well of Ascension
    - The Hero of Ages
    - The Alloy of Law
    - Shadows of Self
    - The Bands of Mourning
    - Mistborn: Secret History
    - The Lost Metal
 - Yumi and the Nightmare Painter
 - Stormlight Archive
    - Way of Kings
    - Words of Radiance

V. E. Schawb
 - Monsters of Verity
    - This Savage Song
    - Our Dark Duet

Neil Shusterman
 - Scythe

Neal Stephenson
 - Snow Crash

Ruth Ware
 - The Woman in Cabin 10

Rebecca Yarros
 - Fourth Wing

## To Be Read List

List of books I'm looking to read in no particular order:

- Empire of the Damned - Jay Kristoff
- Iron Flame - Rebecca Yarros
- The Only One Left - Riley Sager
- Silent Patient - Alex Michaelides
- Ordinary Monsters - J.M. Miro
- The Perfect Marriage - Jeneva Rose
- Bright Young Women - Jessica Knoll
- Delicate Condition
- Five People You Meet in Heaven - Mitch Albom



