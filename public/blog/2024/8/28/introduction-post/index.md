+++
title: Introduction Post!
desc: Freeing myself from the confines of big business
date: Wed, 28 Aug 2024 20:00:00 EDT
+++

# Welcome to the Mikey Zone - Population: Fun!

Hello! This is the intro post for my site! I'm trying to make this website using my friend's cool Racket project: Zeus (named after the Divine Arsenal). Goals for this site are:

- Post about video games, books, and other interests
- Learn more about making and managing static sites
- Get better at version controlling my projects

It's going to be a journey for sure!

## Why am I making this?

In my year of moving away from closed source software operated by mega corporations, this is just another step along the way. See I like writing things and sharing it with my friends. Rather than tossing it up on a social media site owned by Meta/Facebook, I turned to writing markdown docs in a Nextcloud server run by friend. However when sharing that outside of the owner, I was reliant on using things like Google Drive to upload a PDF copy of my writings, and creating a shareable link for these friends to see. It was a step in the right direction but ultimately didn't make too much of a difference.

Now comes this site. Rather than having to share multiple links per file around, I can instead just share the link to the my personal site where you they can view the blog post, or navigate the site to view all the blog posts!

## Potential Upgrades:

As it stands I'm currently using Gitlab Pages to host my site. While this is a step in the right direction for my open sourced journey, I will also want to break free of this one day. But as it stands, I have almost *zero(0)* idea of what I'm doing here. I don't know how to make websites! I barely know how to manage a Git repository! Maybe one day I can swap to being hosted on the friend's server instead of here, buy my own domain name, etc. But I really want to become self-sufficient with the workflow first.

## Closing Thoughts:

Welp that's the post. This will likely not stick around at all, but I needed a post to test out and see how it works in the grand scheme of this!!!!! ENjoy! Buckle up!
