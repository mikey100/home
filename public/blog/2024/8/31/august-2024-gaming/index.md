+++
title: August 2024 Gaming
desc: The games I played in August 2024
date: Sat, 31 Aug 2024 00:00:00 EDT
tags: gaming-list, gaming, 2024, review
+++

## Sniper Elite 4 - (2017) - (4.5/5)
![](sniper_elite_4.png)

Sniper Elite 4 is a military shooter game with an emphasis on sniping Nazis. You play as Karl Fairburne, an OSS operative which translates to one-man army. This time Karl finds himself in Italy where he's uncovering yet more Nazi schemes and weapons of mass destruction that he must sabotage. Sniper Elite 4 features a campaign containing small open world maps with numerous objectives. You'll find yourself in forests, towns, docks, and fortresses as you primarily sneak around, eliminating key targets, collecting documents and intel, and sabotaging weapons. Unlike other games that are

As the name implies. Sniper Elite 4 has a heavy focus on sniping targets from afar with scoped rifles. This game features numerous mechanics to aid you and expand upon something as simple as pointing your weapon and shooting. With Karl being a lone operative (unless playing in co-op mode), you'll tend to rely on stealth and picking off your targets from a safe distance/location. To facilitate this play style, you'll find a plethora of weapons, traps and tools. Before engaging the enemy, you can pull out your binoculars to mark targets, highlight objectives and environmental elements like explosive barrels or dangling pallets on a crane. You can place trip mines, booby trap bodies, collect silenced ammunition, and even create "sound masks" to hide the sound of your rifle firing and not giving away your position. When you pull of certain shots, the game's camera focuses in on the bullet as you watch it travel and hit your mark with an x-ray on your target to show the damage you've done. It honestly never gets old as you see the numerous organs you can single out explode, or watching a bullet pierce one enemy and hit the next. My only complaint with this game is there seems to be a lot of DLC, whether that's additional missions or weapons. But that is easily forgotten as the missions have a good number of objectives and can take a while to complete your first time through them.

If you enjoy shooter games, and specifically ones with a focus on popping heads, this is one of the better games to do just that. If you like stealth games, this is also primarily a stealth game. But if you hate the genre because you hate getting caught, it doesn't mean the end of the world as you can relocate and setup again. Also if you just like playing games with a friend, I played this game in co-op and it was an absolute blast. Being able to mark targets for each other, tackle objectives from two different angles, and help pick each other up when someone inevitably misses a shot and compromises the mission. There's even co-op specific missions where one person plays as a ground operative who is supported by the other person giving them sniper coverage. With multiple ways to tackle missions, plenty of collectibles, and different game modes (Such as Authentic Difficulty which challenges you to account for ballistics physics and factors such as gravity and wind to land shots), Fans of the game have a lot to play with here.

## BLACKTAIL - (2022) - (2/5)
![](blacktail.png)

BLACKTAIL is a game where you do some archer stuff in a fairytale kinda world. But I just think this game is boring. The story is told in a cryptic way, the moments don't feel too memorable, and the gameplay mechanics are not very interesting. A lot of the time you'll be running around a small zone shooting enemies with a bow, picking up materials to craft arrows. There's some puzzle gameplay as well but it all is pretty straight forward. I didn't actually finish playing this one and don't really intend to at the moment. I think this is something people are either going to vibe with or just think it's kinda junky.

## Panzer Dragoon: Remake - (1995/2020) - (2.5/5)
![](panzer_dragoon.png)

I've heard nothing but praise for the Panzer Dragoon games, but I also didn't know anything about them. My only memory of these games is at some point I may have played one of them on the OG Xbox, but that's about it. Seeing the game heavily discounted to be about $2 on Steam, I figured I would check it out.

Panzer Dragoon is a rail shooter game. Your dragon advances forward in the level as you shoot down enemies and projectiles coming at you. While the dragon is moving along its track, you can move the dragon around on the screen to dodge attacks and terrain. One unique thing about Panzer Dragoon is the ability to turn your viewing angle, you can face any of the cardinal directions in relation to the dragon you're riding as enemies can linger in any of those spots. I think this is a cool gimmick as not only does it force you to look around and pay attention to your radar for where enemies are, you also can't control the dragon if you're not looking forward which I thought was neat. Can't really steer correctly if you're not looking where you're steering and all.

Otherwise, I do think Panzer Dragoon may have just been a gem of its age. I can't say it was particularly better than any of the other rail shooters I've played. It's got 7 levels, two weapon types (rapid-fire and lock-on), and the story is not really descriptive from just the info presented on screen. The game is styled like an arcade game which is fine to explain it's short length, but In terms of replay-ability I don't think it's very good either. Panzer Dragoon just has the single player, and a few cheat codes that don't really switch things up all that much. High score chasing isn't something that's really there as the only thing you're scored on is the number/percentage of enemies killed (in my one run I didn't get less than a 90% so it's really not that crazy to be able to 100% defeat enemies every stage once you've learned the levels).

It's not bad exactly, and if you can get it for as cheap as I did I would say it's great! Maybe the sequels expand upon it, but the original is too basic for me to really give it much praise in 2024.

## Onimusha: Warlords - (2001/2019) - (4/5)
![](onimusha_warlords.png)

Onimusha is a demon slaying simulation game disguised as a resident evil clone. You play as Samanosuke, a true blue samurai who is looking for his girlfriend that was kidnapped by demons. After getting a demon gauntlet, Samanosuke is able to suck the souls up of his dead foes. He will mostly explore a castle and the castle grounds as you do classic survival horror esque gameplay like finding keys, solving puzzles, and managing limited resources.

The thing that stands out from similar games is the combat. You get multiple samurai demon swords multiple spells, and have a crazy party/dodging system that is hard to pull off but very cool when you do.

## Castlevania Dominus Collection - (2024) - (4/5)
![](castlevania_dominus.png)

Castlevania Dominus Collection is a collection of three separate Castlevania games from the Nintendo DS. These games are Dawn of Sorrow, Portrait of Ruin, and Order of Eccelsia. The collection also includes an arcade game called Haunted Castle which is the very first Castlevania Arcade game. To make these classic games a bit more accessible, it features save states and a rewind feature. So if you come across a particularly hard section or made a lot of progress but found no save room, you can use these features to make sure you don't loss progress. Besides just being able to play all of these older games that were previously stuck on the DS, the collection also comes with extras including character art sheets, concept art, instruction manuals, promotional art, etc in a gallery as well as a music player. Below are how I would rank the games in the collection:

1) Order of Ecclesia
2) Portrait of Ruin
3) Dawn of Sorrow

All in all, the Dominus Collection is a very welcome collection of games saving gamers from paying high prices on the second-hand market to play these titles. Making them widely available on PC and modern consoles after almost 15+ years is awesome, doubly so for a set of games from a console that doesn't always emulate the best.  And the extras like the art are something fans of the game will really appreciate.

## Castlevania: Dawn of Sorrow - (2005) - (3/5)
![](dawn_of_sorrow.png)

Dawn of Sorrow is a sequel to Aria of Sorrow. We follow the same character Soma Cruz a year after the events of Aria. Soma is confronted by a cultist woman who summons monsters that Soma fights off and absorbs their souls. Finding out that this cult is trying to resurrect the dark lord, Soma tracks down the cult to their base to stop this plot.

Dawn of Sorrow continues the gameplay of modern Castlevania with exploring a large castle map, fighting bosses to acquire upgrades to traverse the castle, and collecting gear to increase your stats/power. Just like it's predecessor, Dawn of Sorrow brings back the Tactical Soul system in which Soma can absorb the souls of defeated monsters to gain their abilities. This abilities can be in the form of a projectile, summon, or passive abilities and each monster has a soul to acquire.

Dawn of Sorrow being a DS game tries to make use of the handheld's touch screen. In the Dominus Collection, you use the right stick to move a cursor to act as the touch screen.  In this game, the touchscreen is used for two things: Magic Seals and breaking certain blocks. The block breaking ability comes up so infrequently that it's barely worth mentioning, but Magic Seals are one of the main gimmicks of this game. In order to defeat bosses, you must draw a Magic Seal. In the original DS version, this was drawing with the stylus, in the Dominus Collection it's pressing inputs set to the points you'd draw from/two that automatically draws the lines for you. I was happy with the input option with the Dominus Collection because I had memories of being a dumb kid and unable to draw the seals correctly on the original release. Ultimately this gimmick is really uninteresting and doesn't really add anything to the game, but you will be making these seals every boss fight.

Dawn of Sorrow is a fine game because it's a Castlevania. The baseline is good. However, I think this is the weakest Castlevania game I've played. The big issue I had was the pacing of the game. I feel the game is slower paced and more confusing as the traversal powers try to be more unique (puppet master, block breaking, paranoia) but end up feeling very useless outside of their single applications and don't really speed up your movement around the castle. The weapon scaling also seems oddly placed. You can use souls to upgrade weapons, but I found the gap between when you find the next soul needed to upgrade your weapon to be too wide where you have a period of feeling weaker for too long waiting for that upgrade. I ended up opting to use the game's boomerang weapon type since you can just buy the upgrades with gold and they felt infinitely stronger than anything else because you can move while attacking.

All in all, Dawn of Sorrow is still a good game, but as a sequel to one of the best games in the series? It falls really flat and doesn't really capitalize on the parts of Aria of Sorrow that made it so good. I like to think it's because they had to come up with touch screen gimmicks which took time/people away from planning the other parts of the game.

## Castlevania: Portrait of Ruin - (2006) - (4/5)
![](portrait_of_ruin.png)

Castlevania Portrait of Ruin is another game included in the Dominus Collection. Portrait of Ruin finds you in control of two characters that you swap between. You explore a big castle as well as entering in painting worlds to smaller more linear levels as you fight enemies, bosses and unlock more abilities to traverse these levels, or equipment to reach new heights of power.

[Check out the full review here!](/blog/2024/9/6/castlevania-portrait-of-ruin)
