+++
title: 2024 Gaming List
desc: Games I've played in 2024 with reviews
date: Mon, 1 Jan 2024 00:00:00 EDT
thumbnail:
thumbnail_desc:
+++
Scoring:

Rough outline of scoring and my thoughts. Putting a score to a go with my opinions are hard and inconsistent. There are exceptions to some of the bullet points but the bare minimum of a game I was able to stomach from start to credits deserves at least a 2 in that it’s playable.

Beyond that is where it gets wishy washy. I would also say a games rating was about my full experience with it since something that is objectively good or bad doesn't mean you can't subjectively enjoy it regardless. So while a game might be one most people would review differently for better or worse, my score is influenced and biased towards factors beyond the game itself.

- 5- Great game, would recommend, would play multiple times, would attempt 100%. Little to no faults with the game. If there are faults, you are usually able to overlook them.
- 4- Good game, would recommend, maybe want to play more than once, would attempt 100%. The difference between this and a 5 would be that a 4 has some sort of fault that is hard to overlook.
- 3- Okay game, might recommend, probably wouldn’t play again. Not a bad experience, but I’ve likely played something better.
- 2- Bad game, wouldn’t recommend, would not consider playing again, others may like it but not for me.
- 1- Terrible game, wouldn’t recommend, wouldn’t even finish it once. Would be surprised if others liked it.

Games are ordered by when I started it up, not indicative of the month I actually finished playing the game.

## January

### <u>ZeroRanger (2018) - (4/5)</u>
> ZeroRanger is a shmup arcade shooter indie game. This genre is typically brutal to get into, but ZeroRanger does a very good job at easing you into this world. As you play and likely fail, you’ll unlock more lives/continues so you’ll have more opportunities to advance while making mistakes. There’s 8 distinct levels in the game, and unlike many others in the genre you’re able to pick up at the start of any level also allowing you to advance. For the most part, it was these features that allowed me to keep playing instead of feeling frustrated at the inherent difficulty and was a welcome change of pace from others in the genre.

> The game also has multiple ways of playing the base game. You have the choice of two fighters to pick from each with their own variant of the 7 different weapons in game which feel pretty different from one another (both the weapons themselves and the character variants of each). This leads you to either approaching specific levels with different weapons, or finding weapons that suit your playstyle and mastering them.

> Another noteworthy piece that makes this game rule is the music. It always finds a way to fit the mood perfectly, hyping you up when you need to most or encouraging you to keep fighting and overcome that hurdle. The minimal color palette is used to great effect as the sprite work is beautiful and detailed even with its limitations.

> Now to get more in depth in my thoughts for the game, I will be going into SPOILER TERRITORY HERE!!!!!!!!!. I think this game is best experienced blind as was recommended to me so I advise if anything I said above appeals to you, to check out the game on your own.

> Alright NOW FOR THE SPOILERS!!!!!!! The game was pretty difficult. This is by design where the accessibility features are your training wheels to ease you into the game and to train you at getting better. You unlock more continues the more you play as your points fill a meter to unlock those continues. This leads to you maybe having to play the same levels a bit where you’ll learn the layout better, remember the enemy patterns, and find the best approach if you want to swap out weapons. This is further enhanced with the game being split into two distinct acts. The last 4 levels are harder variants of the first four levels. Now the real training method though is the absolute ending of the game. On your path to enlightenment you must give up your earthly possessions. After you defeat the final boss you’re presented with a choice. You can wager your save file and progress to attempt a true final boss. This was both really interesting but also infuriating to me.

> On one hand, this is a really cool way to force you to get better. Because if you fail that true final boss, you’re back to the very beginning so you can’t continue from the last level. However the one thing the game can’t reset is your gamer skills. And you will get better from playing over and over. What once took me all my continues to get through a single level, I would be clearing multiple levels with one life. The other hand though, the true final boss I feel deviates a bit from the norm of the gameplay and feels a little unfair to only get that one shot before having to go through an hour of gameplay to get back for another attempt. This seems to be a point of contention for many that have played this game.

> THIS CONCLUDES THE SPOILERS!!!!! Overall I think this was a great gaming experience. I enjoy the genre but rarely play it because I feel I am too bad at them and get intimidated from wanting to play. While this one was definitely not a walk in the park either, ZeroRanger is perfectly crafted to help you get over those fears and make you a better gamer in the process. If you like challenging games, are interested in deceptively simple games with lots of depth, or have ever seen a YouTube video of “World’s Hardest Video Game Boss” and saw a bullet hell wondering to yourself, “What the heck am I watching and how does anyone even do this?” then ZeroRanger is the game to check out. Otherwise do yourself a favor and listen to the soundtrack, it slaps.
