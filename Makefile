build:
	racket zeus/Zeus.rkt build
test:
	raco test -x .
clean:
	rm -rf beta
	rm -rf public
deploy:
	scp -r * alienguix:~/Selfhosted/ste5e.site
local:
	racket zeus/Zeus.rkt -c config_local.json build

