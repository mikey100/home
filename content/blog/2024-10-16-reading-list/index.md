+++
title: Mikey's Reading List
desc: A list of all the books I've read.
date: Wed, 16 Oct 2024 13:12:21 EDT
tags: reading, books, literature
+++

In 2018 I found myself wanting to start reading for fun. If it wasn't obvious, [I play a ton of videogames!!!](/gaming_list/2024/8/28/2024-gaming-list/) So I started reading on and off until recently where I'm now an avid reader. I'm a big fan of fantasy novels (much to my dismay at times), but also enjoy reading thriller and horror novels as well.

Rather than score these like I do with the game lists, I am leaving it open here so I can instead write blog posts about my thoughts on these books in a different way such as giving my takes on a series as a whole, or grouping similar books in a genre to compare and contrast.

Below is the list of all the books I've read organized first by the author's last name, then by year it was published. I will also try to keep books in a single series grouped together.

# The Reading List:

## Alice Feeney
 - Rock Paper Scissors

## Lucy Foley
 - The Guest List

## Colleen Hoover
 - Verity

## Lisa Jewell
 - Then She Was Gone

## Marcus Kliewer
 - We Used to Live Here

## Jay Kristoff
 - Empire of the Vampire
 - Empire of the Damned

## Shari Lapena
 - The Couple Next Door

## Cixin Liu
 - The Three-Body Problem

## Claudia Lux
 - Sign Here

## Sarah J. Mass
 - Throne of Glass
    - The Assassin’s Blade
    - Throne of Glass
    - Crown of Midnight
    - Heir of FIre
    - Queen of Shadows
    - Empire of Storms
    - Tower of Dawn
    - Kingdom of Ash
 - A Court of Thorns and Roses
    - A Court of Thorns and Roses
    - A Court of Mist and Fury
    - A Court of Wings and Ruin

## Freida McFadden
 - Do Not Disturb
 - Ward D

## Tamsyn Muir
 - Gideon the Ninth

## Terry Pratchett
 - Color of Magic

## Taylor Jenkins Reid
 - Daisy Jones & the Six

## Riley Sager
 - Lock Every Door
 - Middle of the Night

## Nicola Sanders
 - All The Lies
 - Don't Let Her Stay

## Brandon Sanderson
 - Mistborn
    - The Final Empire
    - The Well of Ascension
    - The Hero of Ages
    - The Alloy of Law
    - Shadows of Self
    - The Bands of Mourning
    - Mistborn: Secret History
    - The Lost Metal
 - Yumi and the Nightmare Painter
 - Stormlight Archive
    - Way of Kings
    - Words of Radiance

## V. E. Schawb
 - Monsters of Verity
    - This Savage Song
    - Our Dark Duet

## Neil Shusterman
 - Scythe

## Neal Stephenson
 - Snow Crash

## Ruth Ware
 - The Woman in Cabin 10

## Rebecca Yarros
 - Fourth Wing

# Currently Reading list:

- Oathbringer - Brandon Sanderson
 
# The To Be Read List:

List of books I'm looking to read in no particular order:

- House of Leaves - Mark Z. Danielewski
- At the Mountains of Madness, Shadow Over Innsmouth, etc.- H.P. Lovecraft
- Iron Flame - Rebecca Yarros
- The Only One Left - Riley Sager
- Silent Patient - Alex Michaelides
- Ordinary Monsters - J.M. Miro
- The Perfect Marriage - Jeneva Rose
- Bright Young Women - Jessica Knoll
- Delicate Condition
- Five People You Meet in Heaven - Mitch Albom
- The Warden - Daniel M. Ford
