+++
title: Game Review - Castlevania: Order of Ecclesia
desc: Ghosts, Ghouls, and Glyphs
date: Sun, 15 Sep 2024 12:35:22 EDT
thumbnail: order_of_ecclesia.png
thumbnail_desc: Castlevania: Order of Ecclesia
tags: gaming, 2024, review
+++

The third Castlevania title on the Nitendo DS and my final game playing the [Castlevania: Dominus Collection](https://store.steampowered.com/app/2369900/Castlevania_Dominus_Collection/). After playing what felt like a disappointing game in Dawn of Sorrow, and better game in Portrait of Ruin, my experience and knowledge of Order of Ecclesia prior to playing it was minimal. I knew it was a relatively well received game, and had a black haired woman as the protagonist named Shanoa. Did I save the best for last? Would it be the crowning champ of this collection? These were the thoughts in my head booting it up.

# Basics:

![Feel right at home fighting big skeletons and ghost blobs. Classic Castlevania!](big_skeleton.png)

Being another entry into Castlevania, we get the usual platforming action game with RPG elements. What is different in this game compared to the more recent entries is we don't find outselves exploring a giant maze like map where we're limited where we can go based on our traversal abilities. Instead, Order of Ecclesia takes place across multiple smaller levels.

# Plot:

Shanoa belongs to a group known as the Order of Ecclesia who are trying to research countermeasures against Dracula and his eventual return. She is chosen as the human vessel to house magical Glyphs based on Dracula's power called "the Dominus". During the ritual to absorb and bind the Glyphs to herself, Shanoa's colleague, Albus, interrupts the ritual stealing the Dominus for himself rending Shanoa unconscious where she awakens without her memories or emotions. Shanoa is trained again in how to use Glyphs and sent out to find Albus and retrieve the Dominus.

This is a departure from previous games where most of them involve Dracula's Castle appearing and our heroes are exploring it either to confront and defeat Dracula, or stop a force trying to ressurect him. In previous games, we know exactly what needs to be done and why. With Shanoa losing her memories, we're left with an air of mystery. Why did Albus betray us? What is he trying to accomplish? What exactly is the Dominus used for? For the majority of the game we're exploring regions to track down Albus, and sometimes confronting him where he gives us vague hints that the Order of Ecclesia might have more happening beneath the surface.

# Gameplay Overview

Shanoa plays similar to what we've seen in games past, but with subtle differences make this game feel unique thanks to this game's gimmick: the Glyph system. Shanoa doesn't equip weapons like previous protagonists in the more recent Castlevania entries. Instead she equips Glyphs which are either weapon attacks, spells attacks, summons, buffs, or other mechanics. When you use any of these Glyphs, they require a cost of mana, with the basic weapon attacks like a rapier stab being relatively low cost where you're probably regenerating enough mana to spam the ability without concern. However, since Shanoa can equip two Glyphs to be her basic attacks, what this leads to is the ability to use two of the same Glyph and spam that attack rapidly, burning through your mana for huge damage output. In addition, if you have two compatible Glyphs (usually the same one), you can spend Hearts to do a combination attack which would be a big attack similar to what the Glyphs already do. For example, when using it with the axes, you swing a big axe that takes up a lot of the screen and does a lot of damage.

![What's yours is mine...](glyph_steal.png)

Similar to the Tactical Soul system found in Aria/Dawn of Sorrow, Shanoa can defeat enemies to collect Glyphs. She must draw in the Glyphs (performed by holding Up), to absorb them and learn them but once learned they are added to your arsenal of abilities. Defeating certain enemies has a chance for specific Glyphs to spawn for Shanoa to absorb, as well as statues being present in maps that contain Glyphs as well. A neat mechanic is the enemies you face also use Glyphs, so when you see them start to use a Glyph you can absorb and steal their spells. This becomes very cool as some bosses also use Glyphs allowing you to gain some really powerful abilities.

![Shanoa and Nikolai discussing plot, circa 1800](village.png)

Unlike previous entries, Order of Ecclesia doesn't feature a giant castle like we're used to. Instead it's a bunch of smaller more linear levels, with a village acting as a hub world of sorts. As you explore these maps, you'll also start rescuing the villagers that were kidnapped and trapped by the game's antagonist. These villagers unlock sidequests for you to complete to get better gear, money, and unlock better items in the shop for you to purchase. These quests can be simple like kill 30 crows, go to a previous location and use an item, or take a picture of a certain enemy. Besides needing to find all the villagers for the game's good ending, having the sidequests for upgrades was a good way to extend the game. Another way to extend the gameplay for me was farming enemies for their item drops.


# Verdict:

![Slinging spells and raising hell](zap.png)

I think Order of Ecclesia is a stellar Castlevania game. The Glyph system is a lot of fun as you are able to mix and match attacks, and swap between sets of Glyphs on the fly to easily switch your tactics as needed. And with so many Glyphs to try out, you'll likely find a combination that works for you that other players may not have thought to try out.

While it was a welcome change of pace at first and I really didn't mind the smaller levels, you will eventually have the revelation where you realize almost half the maps have counterparts that are the same map but reversed. Castlevania isn't a stranger to inverting/reversing maps, but in previous games where this is a thing, the flipside usually feels like an entirely new map on its own. In Order of Ecclesia, the smaller maps are linear so putting in reverse really doesn't add anything, it really just detracts from the sense that the maps are unique areas.

Another issue I had with this game was the gear/item progression. I found that most of the game I spent wearing the same gear as I couldn't really find meaningful upgrades over what I already had equipped. This lead to me grinding a lot of the enemies for all their item drops to see if any of them had gear upgrades. This also lead me to find just how all over the place item drops can be as some later game enemies were dropping mid game items needed to complete quests I've already done.

Issues aside, I do think Order of Ecclesia is one of the better Castlevania games I've played. It feels like it looked to the past games and took the elements that worked for those games to make it's strengths even stronger.Even  if some of the new ideas like the linear levels didn't work out in its favor, a Castlevania game is still a Castlevania game. The fantasy gothic theming, accompanied by great art and music will never get old for me.

I would definitely recommend playing Order of Ecclesia. If you like platformer games, action RPGs, or want to delve into a piece of a longstanding series history, this is definitely one to consider.
