+++
title: Introduction Post: Meet the Maker
desc: Main Character writes his intro.
date: Wed, 28 Aug 2024 20:00:00 EDT
+++

# Welcome!!!

Hello! First off, thanks for checking out my website! Chances are if you're reading this you already know me, but here's a quick introduction just in case:

- My name is Mikey.
- I play A LOT of video games (all genres, preferably retro or indie games)
- I listen to music and collect vinyl records (I like a lot of genres but tend to stick towards the rock spectrum. I love punk rock specifically. My favorite artist is Anamanaguchi)
- I read books (mostly fantasy, horror, and thrillers)
- I like doing arts/crafts (digital drawing, crocheting, etc.)
- I'm a career "IT guy" (I help people use their computer and fix their problems)
- My perfect day is hanging/vibing with my friends and family

## Why a website/blogsite? 

I plan on using this site primarily for catalouging the video games I've played, but there will be other things such as video game reviews, my reading list, and blog post writings. Think of this website as my journal/diary of sorts, taking a peek behind the Mikey curtains so to speak! 

I like writing things and sharing it with my friends, but I'm also trying to move away from closed source software operated by mega corporations. Originally this started as a Google Drive document that listed the games I played in a year starting in 2022. So my first step was getting off Google. This was done by writing markdown docs in a Nextcloud server run by my friend, Stove. However when sharing my stuff outside of that Nextcloud server, thing got harder. I was still reliant on services like Google Drive to share the documents around, and was not about to start posting on social media sites like Facebook/Instagram/Twitter. So now we have this website! Rather than having to share multiple links per file around, I can instead just share the link to the my personal site where you they can view the blog post, or navigate the site to view all the blog posts! It's not perfect as I'm hosting it through Gitlab so I'm reliant on a company, but (hopefully) that's better than leaving it in Facebook or Google's hands.

This has also been a bit of a learning experience for me. As I'm learning/reinforcing some skills like version controlling through Git, learning to navigate and act within a Linux terminal, and HTML/CSS practices. I'm also using it to test drive Stove's static site generator: Zeus (Named after the AA-Divine Arsenal, not the Greek God!). Ideally I can help him with documenting the software by being closer to a "normie" user who may want to use this.

## Potential Upgrades:

As it stands I'm currently using Gitlab Pages to host my site. While this is a step in the right direction for my open sourced journey, I will also want to break free of this one day. But as it stands, I have almost *zero(0)* idea of what I'm doing here. I don't know how to make websites! I barely know how to manage a Git repository! So ideally my next project for this site will be learning to host my own web server on something like a raspberry pi, and then buying my own domain.

I'm also "borrowing" a site template from Stove and have been making tweaks to differentiate it from his. Eventually I need to have my own creative leaps that can make our sites look a bit more distinct from one another.

## Closing Thoughts:

Welp that's the post. Let's see where this journey takes me. Enjoy! Buckle up! Until Next Time! And above all, thank you for reading :)
