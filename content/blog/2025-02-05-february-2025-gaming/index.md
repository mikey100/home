+++
title: February 2025 Gaming
desc: The games I played in February 2025
date: Wed, 5 Feb 2025 12:44:53 EST
tags: gaming-list, gaming, 2025, review
+++

## Tomb Raider: Underworld - (2008) - (3/5)
![](tomb_raider_underworld.png)

The sequel to Tomb Raider: Legend. This game in a lot of ways plays/feels better, but I think the levels feel overall worse than Legend. That being said, the two games are very similar.

## CrossCode - (2018) - (x/5)
![](crosscode.png)

Review goes here.

## Dungeons of Dreadrock - (2022) - (3/5)
![](dungeons_of_dreadrock.png)

Review goes here.

## Ape Escape 3 - (2005) - (4/5)
![](ape_escape3.png)

Better than Ape Escape 2!
