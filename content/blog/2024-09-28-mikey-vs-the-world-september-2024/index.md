+++
title: Mikey vs. The World: September 2024
desc: Monthly life update and check-in
date: Sat, 28 Sep 2024 15:16:50 EDT
tags: personal-life, 2024
+++

# What's happening???

It's ya boi Mikey here, and with another edition of Mikey vs. The World. I say another but this is the first edition! While writing this I listened to a 30 minute loop of [Suburban Tram by Anamanaguchi](https://www.youtube.com/watch?v=kh1QUicFl8s) and I thought it'd be fun to include that as a sort of soundtrack for the blogpost (also helped me come up with a name for these updates!).

This month has kinda flown by but a lot has happened. So let's dive right in and see what the month of September held for me.

# Mikey100 - My Personal Website

Firstly you're likely reading this on one of the latest additions to my life, my personal website! I use the moniker of Mikey100: a combination of my name, "Mikey" and the number, "100". It was inspired by an Anamanaguchi song, [Rox 300](https://www.youtube.com/watch?v=Mf_luOSqOtQ) as well as at the time I was trying to 100% a lot of the video games I was playing. I'm not ready to let go of 99 Fishing Mikey on a lot of my socials, and wanted Mikey100 to be my "content creation" username when YouTube/Streaming was a hobby I wanted to try (didn't work out because of concerns/fears for internet privacy and I feel like I wouldn't dedicate time to it).

This is a project I've always had an interest in but was always back and forth on if I wanted to go through with it, but I found some new motivators that pushed me along. I'm a believer that creative works, and personal projects are something that should have value with validation from outside sources. If you create something, there's value intrisically:

- the time or effort put into creating it
- the thoughts and motivation to manifest it into being
- and the knowledge and experience gained from the process.

Whether you're happy with your final product is completely separate from the worth gained from completing a project.

I created this website to host these writings of mine. This is my journal, diary, blog, whatever you want to call it, and it's here for me to collect and reflect. But I also had some people interested in reading my writings (especially when I mention I write reviews of the many many video games I play each year), and to share it with them wasn't always convenient. So rather than sharing a PDF with them in a few different ways, my friends can instead open my website on their web browser and read it at their leisure with ease. I'm appreciative of anyone who invests time no matter how much into looking at these web pages, and if they gain something out of reading my thoughts/opinions that's even better. But this is for me first and foremost. It's a project to organize my thoughts, it's a project to learn a bit more about website development, it's to aid my friend's static site project with providing user experience outside of his own, and it's an outlet for expressing myself.

# Social Events AKA Every Saturday in September

Looking at my calendar, I've had something planned every weekend this month. Breaking it down in the order it happened:

- Boys Night LAN Party for Deadlock
- Metal Concert seeing the invincible "Mudvayne"
- Friend's Sister's Engagement Party (Really a wedding)
- Friend's Baby Shower

There were something other things I went to but these were my Saturdays this month and more highlight-able.

## BOYS NIGHT LAN PARTY!!!
> The LAN Party was honestly a ton of fun! I've really become mostly a single player gamer, and it's been a while since I was able to find time with the boys to play a game together. Not for lack of trying just sometimes it's just hard to find a game we're all interested in, a game we are okay with taking multiple sessions, or a game I'm knowledgeable enough in to get done in a single session. And sometimes we're just tired and want to chill, sip on some beers, and play children's card games. But this LAN party featured the latest competitive multiplayer game from Valve, Deadlock. We locked and loaded into my office, played for like 8 hours straight until 3am, and then hung out a little longer until 4am before calling it quits. I can safely say I don't need to play Deadlock ever again! Even if I'm not crazy about the game itself, it's rare to get that kind of gaming experience these days with just hanging with you friends all jamming together on the same thing. Makes me long for the art nights I used to have, and made me finally understand why I enjoy in person Dungeons and Dragons. Just good company all collectively engaging with something without forcing conversation or jumping to cellphones for instant gradification.

## The Invincible Mudvayne LIVE
> The metal concert I don't have much to say about to be honest. I had a good time hanging out in the parking lot eating fast food burgers, drinking some beers, and playing some games. I'm not super into metal music anymore like I was in my teenage years, but I can still appreciate it. I got to see All That Remains, Mudvayne and Megadeth. This was my first time seeing All That Remains, and they've got some hits I think are fine like "Two Weeks" and "Six", but their short setlist was appreciated because anymore I would have been bored. Mudvayne though I think is phenomenal. If you're not into metal but want to know what it's like, I think Mudvayne puts on that perfect show. They wear crazy makeup, they've got really good metal songs, and they put on a good performance. I wish Mudvayne was the headliner because getting extra Megadeth time was definitely not for me. I've never been a big Megadeth fan as I found them less exciting than the other "Big Four" metal bands (Megadeth, Metallica, Antrhax, and Slayer). But it was nice to see such an old band still play and perform well, and seeing their hits live was nice. This was technically my second time seeing Megadeth but I really don't remember much of the first time.

## Engagement party that was actually a wedding
> My friend's sister and her husband decided to have an engagment party. Might be weird to read that since husband should probably read fiance. But in this case this couple was already married. For "reasons", they decided to get married in Las Vegas last year, but I guess a year later felt they didn't really get to celebrate it with friends or family so they threw an engagement party. I'm in the camp that traditional weddings are way too expensive and not always easy for the couple getting married to enjoy, so I respected the decision to do something a little different. It was nice enough, but I found myself bored since my friend and his fiance were busy being family members of the bride at a wedding, and I didn't really know anyone else there. I made the most of it casually sipping espresso martinis and burning matches in the parking lot (IYKYK). Also easily the worst part of this was that it was at a big German Resturant/Biergarten and the wedding DID NOT SERVE PRETZELS!!! That's a crime!!!

## Baby shower that I did not attend
> Unfortunately a few days before this event (which is happening today the 28th!), I woke up not feeling particularly well. Due to seasonal allergies, I felt a mix of symptoms that are adjacent to COVID-19. I did not test positive for COVID and I honestly feel pretty fine now with the exception of a sore throat, but I didn't want to risk going and making people uncomfortable. I sent my regards to the future mom and dad via text, but I do feel bad that I probably won't have an opportunity to see them before they are shackled to parenthood and all the restrictions that come with it. Hopefully when they settle into their life with their child we can find time to get together.

# Short things of note:
On top of those events where I had something to write about, here's some quick things of interest that happened this month as well:

- A noise on my car prompted me to get it inspected a month earlier than it's due. Driving around, there was a loud droning sound that changed pitch based on my speed. After changing tires and the noise still existing, I had brought it back to the mechanic. Turned out to be worn down rear wheel bearings. When I first searched online for an answer to my problem, the rear wheel bearings were mentioned a few times and apparently it's common for Subaru models around my car's age (supposedly my mechanic's shop had done this exact job on 2 Subaru's in the last week).  Way too much money later, the noise is gone along with anxiety of being a loud obnoxious car!

- I've been reading [Brandon Sanderson's "Stormlight Archive"](https://www.brandonsanderson.com/pages/the-stormlight-archive-series), an epic fantasy series spanning multiple books with multiple storylines, perspectives, a unique magic system, and what not. Each book is about 1000+ pages so it's been my main reading. I like it a lot but wish I could put it down so I can get some variety. But I'm just so curious of where the story goes that I find it hard to stop! Borrowing the audiobook from my library has helped my progress as I can listen while I am driving or exercising and pick it up on my phone's e-reader when I'm able to sit down and read.

- Started checking out a podcast called [CreepCast](https://www.youtube.com/@CreepPodcast) at the recommendation of a few friends. Two YouTubers (Wendigoon and MeatCanyon) read and talk about CreepyPasta's and other shortform horror stories. Very fun to listen to and some of the stories are very well written. Definitely recommend checking it out if you're looking for something close to a book club experience with Mystery Science Theater 3000-esque commentary at times.

- I'm down about 10 pounds from the last time I weighed in. I really don't know when the last time I weighed myself was since I don't do it often, but feeling good about it. Definitely notice my current body size being in between clothing sizes which makes getting dressed a little annoying at the moment, but that's a good issue to have.

# What's coming up on the October docket?
In October I already have plans the first two weekends. I'm actually taking off a week and a half from work to drive upstate to visit my parents, and some friends. I'm going to be driving a good bit so I wanted to get that car squared away before this trip. I've got 3 to 4 hours to the first destination and then about an 1 to 2 hours between the other stops. I really need a break/getaway from daily life. Work has been grinding me down and with an uptick in labor, and corporate nonsense that really has me feeling burnt out. I'll be driving by myself but I look forward to that. I enjoy long car rides by myself as I get to be alone with my thoughts. Going to crank out some more Stormlight on Audiobook but also get to jam out by myself!

As far as gaming goes, I'm looking forward to playing [Metaphor: ReFantazio](https://store.steampowered.com/app/2679460/Metaphor_ReFantazio/). I'm craving a JRPG, and ATLUS always delivers enjoyable JRPGs. Not looking forward to the length as it's likely going to be a 60+ hour game. On top of that, a new indie game called [Flowstone Saga](https://store.steampowered.com/app/1372000/Flowstone_Saga/) caught my eye as it's a JRPG but you play Tetris instead of turn based battles. I've also got some other games in my library that I should play instead of buying new games (the car maintainance bills definitely stung) so I'll maybe check those out next month.

I've got a hankering to crochet some more coasters for my home and as gifts for people. I don't crochet often enough so these simple projects tend to take me a bit to get done. I figure if I make more coasters not only will I have more coasters for use in my home, but I think it's a nice thing to include in gifts for people and with the holidays only a few months away, I've got excuses to give gifts. Just starting a new project whether it's a game, or creative one is always hard for me to take that first step! Hopefully by next post I can have some pictures of the end results.

I've also got a backlog of books and most of them are fantasy novels so I don't know when it's getting done. I think I need to read some psych thrillers to break out of fantasy for a little bit and further prolong that backlog. With the spooky holiday coming up, I think it'll also fit the season. That and reading ["Empire of the Damned", the sequel to "Empire of the Vampire"](https://jaykristoff.com/books/empire-of-the-vampire/) will be thematically appropriate.

# Closing Remarks
That wraps it up for the first issue of Mikey vs. The World! I don't know if this is going to be a monthly publication of mine. I don't know if enough things change month to month where it'll be noteworthy, but I needed to start somewhere. I'll start keeping notes for an October edition and if it's looking light, I will combine with November. It was nice to have a small recap session for myself to type all out.

As always, if you've made it this far thanks for taking the time to read this! I appreciate you! :)




