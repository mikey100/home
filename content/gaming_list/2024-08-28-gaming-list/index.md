+++
title: 2024 Gaming List
desc: List of my 2024 video games
date: Wed, 28 Aug 2024 20:00:00 EDT
tag: gaming-list, 2024
+++

# Games I played in 2024 with reviews

Games are ordered by when I started it up, not indicative of the month I actually finished playing the game. Click the headers to see the quick reviews of each months games. Game titles with a link have a more in-depth review.

Items should be read as: Game Title - (year of release/year of remake) - (score out of 5)

Scoring:

Rough outline of scoring and my thoughts. There are exceptions to some of the bullet points but the bare minimum of a game I was able to stomach from start to credits deserves at least a 2 in that it’s playable.

Beyond that is where it gets wishy washy. I would also say a games rating was about my full experience with it since something that is objectively good or bad doesn't mean you can't subjectively enjoy it regardless. So while a game might be one most people would review differently for better or worse, my score is influenced and biased towards factors beyond the game itself.

- 5 - Great game, would recommend, would play multiple times, would attempt 100%. Little to no faults with the game. If there are faults, you are usually able to overlook them.
- 4 - Good game, would recommend, maybe want to play more than once, would attempt 100%. The difference between this and a 5 would be that a 4 has some sort of fault that is hard to overlook.
- 3 - Okay game, might recommend, probably wouldn’t play again. Not a bad experience, but I’ve likely played something better.
- 2 - Bad game, wouldn’t recommend, would not consider playing again, others may like it but not for me.
- 1 - Terrible game, wouldn’t recommend, wouldn’t even finish it once. Would be surprised if others liked it.
- x - I started and either didn't finish the game or play enough to get a solid idea how I should score it.

If I gave a score a that includes a .5 in the number, it's either I liked it more than similarly rated games but not enough to give it a whole number increase. Or I gave the game a score originally after I felt I got enough experience to score it, and then the score changed after completing the game. An example is a game that started strong I may have given a 5 but by the end of it, I realized it wasn't exactly a 5 for me but I still enjoyed it enough at some point to consider it a 5.


## [January](/blog/2024/1/31/january-2024-gaming)
 - ZeroRanger - (2018) - (4/5)
 - The Tartarus Key - (2022) - (3/5)
 - Secret of Mana - (1993/2018) - (4/5)
 - Grim Legends: The Forsaken Bride - (2014) - (4/5)
 - Astalon: Tears of the Earth - (2019) - (3/5)
 - Mad Max - (2015) - (3/5)
 - Momodora: Moonlit Farewell - (2024) - (3/5)
 - Like a Dragon: Infinite Wealth - (2024) - (4/5)

## [February](/blog/2024/2/29/february-2024-gaming)
 - Children of Silentown - (2023) - (3/5)
 - ASTLIBRA Revision Gaiden: The Cave of Phantom Mist - (2024) - (3/5)
 - Last Epoch - (2024) - (2/5)
 - Dead Island Riptide - (2013) - (3/5)
 - Marvel's Midnight Suns - (2022) - (4/5)
 - Titan Quest Anniversary Edition - (2006/2016) - (4/5)
 - Shadow Hearts - (2001) - (3/5)

## [March](/blog/2024/3/31/march-2024-gaming)
 - Sonic Adventure 2 - (2002) - (3/5)
 - Nioh 2 - (2020) - (3/5)
 - Heretic's Fork - (2023) - (4/5)
 - Broken Sword 1 - Shadow of the Templars: Director's Cut - (1996/2010) - (4/5)
 - SaGa Frontier Remastered - (1997/2021) - (x/5)
 - The Incredible Adventures of Van Helsing: Final Cut - (2015) - (3/5)

## [April](/blog/2024/4/30/april-2024-gaming)
 - Stardew Valley - (2016) - (4/5)
 - Legacy of Kain: Soul Reaver - (1999) - (4/5)
 - Legacy of Kain: Soul Reaver 2 - (2001) - (3/5)
 - The Pony Factory - (2024) - (4.5/5)
 - Exiled - (2024) - (4.5/5)
 - Sea of Stars - (2023) - (2/5)
 - Severed Steel - (2021) - (3/5)
 - Bloons TD 6 - (2018) - (5/5)
 - Rusty's Retirement - (2024) - (4/5)

## [May](/blog/2024/5/31/may-2024-gaming)
 - Unicorn Overlord - (2024) - (4/5)
 - Animal Well - (2024) - (4/5)
 - Crow Country - (2024) - (5/5)
 - Soulstice - (2022) - (3.5/5)
 - Quake II - (1997/2023) - (5/5)
 - Machinika Museum - (2021) - (3/5)

## [June](/blog/2024/6/30/june-2024-gaming)
 - Dark Cloud - (2001) - (4/5)
 - Shin Megami Tensei V: Vengeance - (2024) - (5/5)
 - SKALD: Against the Black Priory - (2024) - (3/5)
 - WarioWare, Inc.: Mega Microgames! - (2003) - (4/5)
 - Gato Roboto - (2019) - (3.5/5)
 - Riven - (1997/2024) - (4.5/5)

## [July](/blog/2024/7/31/july-2024-gaming)
 - Sudeki - (2004) - (2.5/5)
 - Life is Strange 2 - (2019) - (3.5/5)
 - Myst - (1993/2021) - (4.5/5)
 - Cryptmaster - (2024) - (2.5/5)
 - Tormented Souls - (2021) - (4/5)
 - Zoeti - (2023) - (2.5/5)

## [August](/blog/2024/8/31/august-2024-gaming)
 - Sniper Elite 4 - (2017) - (4.5/5)
 - BLACKTAIL - (2022) - (2/5)
 - Panzer Dragoon: Remake - (1995/2020) - (2.5/5)
 - Onimusha: Warlords - (2001/2019) - (4/5)
 - Castlevania Dominus Collection - (2024) - (4/5)
 - Castlevania: Dawn of Sorrow - (2005) - (3/5)
 - Castlevania: Portrait of Ruin - (2006) - (4/5)

## [September](/blog/2024/9/30/september-2024-gaming)
 - Castlevania: Order of Ecclesia - (2008) - (4/5)
 - Deadlock - (2024) - (4/5)
 - High on Life - (2022) - (3/5)
 - Blasphemous - (2019) - (4/5)
 - Marvel's Guardians of the Galaxy - (2021) - (3.5/5)
 - Out There Somewhere - (2014) - (4/5)
 
## [October](/blog/2024/10/31/october-2024-gaming)
 - Unknown 9: Awakening - (2024) - (1/5)
 - Planet of the Eyes - (2015) - (3/5)
 - Hexologic - (2018) - (4/5)
 - Metaphor: ReFantazio - (2024) - (3.5/5)
 - CLICKOLDING - (2024) - (2/5)
 - Mouthwashing - (2024) - (4/5)
 
 
## [November](/blog/2024/11/1/november-2024-gaming)
 - The Black Grimoire: Cursebreaker - (2024) - (3/5)
 - Amnesia: A Machine for Pigs - (2013) - (pig/5)
 - Terranigma - (1995) - (3/5)
 - Guild Wars 2 (2012-2024) - (3/5)
 - Metro 2033 (2010) - (2.5/5)
 - Ape Escape - (1999) - (4/5)
 - Myst III: Exile - (2001) - (3/5)
 
## [December](/blog/2024/12/1/december-2024-gaming)
 - SOMA - (2015) - (4/5)
 - UFO 50 - (2024) - (4/5)
 - LIVE A LIVE - (1994/2022) - (4/5)
 - THRESHOLD - (2024) - (3/5)
 - Metro: Last Light Redux - (2013/2014) - (x/5)
 - Bomb Rush Cyberfunk - (2023) - (4/5)
 - 10 Dead Doves - (2024) - (4/5)
 - Parasite Eve - (1998) - (3/5)
 
