+++
title: 2025 Gaming List
desc: List of my 2025 video games
date: Thu, 2 Jan 2025 12:57:26 EST
tag: gaming-list, 2025
+++

# Games I played in 2025 with reviews

Games are ordered by when I started it up, not indicative of the month I actually finished playing the game. Click the headers to see the quick reviews of each months games. Game titles with a link have a more in-depth review.

Items should be read as: Game Title - (year of release/year of remake) - (score out of 5)

Scoring:

Rough outline of scoring and my thoughts. There are exceptions to some of the bullet points but the bare minimum of a game I was able to stomach from start to credits deserves at least a 2 in that it’s playable.

Beyond that is where it gets wishy washy. I would also say a games rating was about my full experience with it since something that is objectively good or bad doesn't mean you can't subjectively enjoy it regardless. So while a game might be one most people would review differently for better or worse, my score is influenced and biased towards factors beyond the game itself.

- 5 - Great game, would recommend, would play multiple times, would attempt 100%. Little to no faults with the game. If there are faults, you are usually able to overlook them.
- 4 - Good game, would recommend, maybe want to play more than once, would attempt 100%. The difference between this and a 5 would be that a 4 has some sort of fault that is hard to overlook.
- 3 - Okay game, might recommend, probably wouldn’t play again. Not a bad experience, but I’ve likely played something better.
- 2 - Bad game, wouldn’t recommend, would not consider playing again, others may like it but not for me.
- 1 - Terrible game, wouldn’t recommend, wouldn’t even finish it once. Would be surprised if others liked it.
- x - I started and either didn't finish the game or play enough to get a solid idea how I should score it.

If I gave a score a that includes a .5 in the number, it's either I liked it more than similarly rated games but not enough to give it a whole number increase. Or I gave the game a score originally after I felt I got enough experience to score it, and then the score changed after completing the game. An example is a game that started strong I may have given a 5 but by the end of it, I realized it wasn't exactly a 5 for me but I still enjoyed it enough at some point to consider it a 5.

## 2025 VIDEO GAME BUDGET TRACKER: $14.39/$200
I'm trying not to spend much money on video games this year and instead play games I already own. Here's my budget tracker to keep myself accountable.

## [January](/blog/2025/1/2/january-2025-gaming)
- S.T.A.L.K.E.R. Shadow of Chernobyl - (2007) - (x/5)
- Tomb Raider: Legend - (2006) - (3/5)
- Atlas Fallen - (2023) - (3/5)
- Ape Escape 2 - (2002) - (3/5)
- The Room - (2012) - (3/5)
- BLADE CHIMERA - (2025) - (4/5)
- Arctic Eggs - (2024) - (5/5)
- Earthbound - (1995) - (x/5)
- Hollow Knight - (2017) - (x/5)
- Forgotten City - (2022) - (4/5)

## [February](/blog/2025/2/5/february-2025-gaming)
- Tomb Raider: Underworld - (2008) - (3/5)
- CrossCode - (2018) - (x/5)
- Dungeons of Dreadrock - (2022) - (3/5)
- Ape Escape 3 - (2005) - (4/5)

# Games I've got in mind to play:
- Afterimage
- Breath of Fire
- Broken Sword 2
- Dread Delusion
- Final Fantasy 8
- Grandia 1
- La-Mulana 1+2
- The Longest Journey
- Metro Last Light+Exodus
- Monster Hunter World (again)
- Ninja Gaiden Sigma 1, Sigma 2, Razor's Edge 3
- Parasite Eve 2
- S.T.A.L.K.E.R. Clear Sky and Call of Pripyat
- Shadow Hearts Covenant
- Wild Arms
- Xenogears
